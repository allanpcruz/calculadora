function addToDisplay(value) {
    document.getElementById('display').value += value;
}

function clearDisplay() {
    document.getElementById('display').value = '';
}

function calculate() {
    try {
        const input = document.getElementById('display').value;
        const sanitizedInput = input.replace(/[^-()\d/*+.]/g, '');

        if (sanitizedInput === '') {
            throw new Error('Expressão vazia');
        }

        const result = eval(sanitizedInput);

        if (isNaN(result) || !isFinite(result)) {
            throw new Error('Erro de cálculo');
        }

        document.getElementById('display').value = result;
    } catch (error) {
        document.getElementById('display').value = 'Erro';
    }
}
